package util;

public class ColorPoint {
	public double x,y;
	public int color;
	
	@Override
	public String toString() {
		return "#"+Integer.toHexString(color)+" ("+x+" , "+y+")";
	}
	
}
