package voronoi;
import java.util.LinkedList;
import java.util.List;

import imagegenlib.generator.IGenerator;
import util.ColorPoint;
import util.PointGrid;



public abstract class VoronoiAbstract implements IGenerator{

	private PointGrid pointGrid;
	
	protected List<ColorPoint> points = new LinkedList<ColorPoint>();
	protected int width, height;
	protected int numpoints;
	
	public VoronoiAbstract(int width, int height, int numpoints) {
		this.width = width;
		this.height = height;
		this.numpoints = numpoints;
	}
	
	protected void initPoints() {
		for(int i=0;i<this.numpoints;i++) {
			ColorPoint point = new ColorPoint();
			point.x = this.getPointPos(this.width);
			point.y = this.getPointPos(this.height);
			point.color = this.getPointColor(point.x, point.y);
			
			this.points.add(point);
		}
		
		this.pointGrid = new PointGrid(this.points,this.width,this.height);

	}
	
	public int getColor(double x, double y) {
		
		return this.pointGrid.getClosestPoint(x, y).color;
	}
	
	protected abstract int getPointColor(double x, double y);
	protected abstract double getPointPos(double max);
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
}
