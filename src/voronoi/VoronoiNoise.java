package voronoi;
import java.util.LinkedList;
import java.util.List;

import imagegenlib.annotate.CanonConstructor;
import imagegenlib.annotate.GenArgName;
import imagegenlib.annotate.GenClass;
import util.ColorPoint;

@GenClass(
		name="Voronoi Noise",
		desc="Creates a mosaic of random shaped and colored tiles"
)
public class VoronoiNoise extends VoronoiAbstract{
	
	
	
	protected List<ColorPoint> points = new LinkedList<ColorPoint>();
	protected int width, height;
	protected int numpoints;
	
	@CanonConstructor
	public VoronoiNoise(@GenArgName(name="Width") int width,@GenArgName(name="Height") int height,@GenArgName(name="Number of tiles") int numpoints) {
		super(width,height,numpoints);
		this.initPoints();
	}
	
	@Override
	protected int getPointColor(double x, double y) {
		return (int)(Math.random()*0x1000000);
	}

	@Override
	protected double getPointPos(double max) {
		return Math.random()*max;
	}
	
}
