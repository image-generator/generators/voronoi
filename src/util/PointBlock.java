package util;

import java.util.LinkedList;
import java.util.List;

public class PointBlock {

	
	private List<ColorPoint> points;
	
	public PointBlock() {
		this.points = new LinkedList<ColorPoint>();
	}
	
	
	public void add(ColorPoint cp) {
		this.points.add(cp);
	}
	
	public List<ColorPoint> getPoints() {
		return this.points;
	}
}
