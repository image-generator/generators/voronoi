package voronoi;
import java.awt.image.BufferedImage;

import imagegenlib.annotate.CanonConstructor;
import imagegenlib.annotate.GenClass;


@GenClass(
		name = "Voronoi Image",
		desc = "Creates a mosaic out of an image"
)
public class VoronoiImage extends VoronoiAbstract{

	protected BufferedImage image;
	
	@CanonConstructor
	public VoronoiImage(BufferedImage b, int numpoints) {
		super(b.getWidth(), b.getHeight(), numpoints);
		this.image = b;
		this.initPoints();
	}

	
	@Override
	protected int getPointColor(double x, double y) {
		return this.image.getRGB((int)x, (int)y);
	}


	@Override
	protected double getPointPos(double max) {
		return Math.random()*max;
	}
}
