package voronoi;
import java.awt.image.BufferedImage;

import imagegenlib.annotate.CanonConstructor;
import imagegenlib.annotate.GenArgName;
import imagegenlib.annotate.GenClass;

@GenClass(
		name = "Voronoi Image Gaussian",
		desc = "Creates a mosaic out of an image, with more tiles to the center"
)
public class VoronoiImageGaussian extends VoronoiImage{

	@CanonConstructor
	public VoronoiImageGaussian(@GenArgName(name = "Base Image") BufferedImage b, @GenArgName(name = "Number of tiles") int numpoints) {
		super(b, numpoints);
	}

	
	@Override
	protected double getPointPos(double max) {
		return max*0.5*(Math.random()+Math.random());
	}
}
