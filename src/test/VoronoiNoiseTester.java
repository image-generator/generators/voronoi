package test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import voronoi.VoronoiNoise;

public class VoronoiNoiseTester {

	public static void main(String[] args) throws IOException {
		VoronoiNoise noise = new VoronoiNoise(1000,1000,1000);
		BufferedImage image = new BufferedImage(noise.getWidth(),noise.getHeight(),BufferedImage.TYPE_INT_RGB);
		
		for(int x=0;x<image.getWidth();x++) {
			for(int y=0;y<image.getHeight();y++) {
				int color = noise.getColor(x, y);
				image.setRGB(x, y, color);
			}
		}
		
		ImageIO.write(image, "png", new File("output.png"));
	}
}
