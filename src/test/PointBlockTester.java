package test;

import java.util.LinkedList;
import java.util.List;

import util.ColorPoint;
import util.PointGrid;

public class PointBlockTester {

	
	public static void main(String[] args) {
		
		final double height = 10;
		final double width = 10;
		final int numpoints = 1_000_000;
		
		
		List<ColorPoint> points = new LinkedList<ColorPoint>();
		for(int i=0;i<numpoints;i++) {
			ColorPoint c1 = new ColorPoint();
			c1.x = Math.random()*width;
			c1.y = Math.random()*height;
			
			points.add(c1);
		}
				
		System.out.println(points.size());
		PointGrid grid = new PointGrid(points,width,height);
		
		ColorPoint c = grid.getClosestPoint(10, 10);
		System.out.println(c);
	}
}
