package util;

import java.util.LinkedList;
import java.util.List;

public class PointGrid {

	private PointBlock[][] blocks;
	
	private double blockwidth, blockheight;
	private int blockxcount, blockycount;
	
	private int numPoints;
	
	public PointGrid(List<ColorPoint> points, double width, double height) {
		this.numPoints = points.size();
		
		//Using regular invsqrt, because this isn't Quake III
		this.blockwidth = (width/Math.sqrt(numPoints));
		this.blockheight = (height/Math.sqrt(numPoints));
		
		System.out.println(this.blockwidth);
		System.out.println(this.blockheight);
		
		this.blockxcount = (int)(width/blockwidth);
		this.blockycount = (int)(height/blockheight);
		
		this.blocks = new PointBlock[this.blockxcount][this.blockycount];
		
		//Initializes point blocks, keep null for better runtime
		for(ColorPoint cp: points) {
			
			int x = this.gridClampX((int)(cp.x/this.blockwidth));
			int y = this.gridClampY((int)(cp.y/this.blockheight));
			
			
			if(blocks[x][y] == null) {
				blocks[x][y] = new PointBlock();
			}
			
			blocks[x][y].add(cp);
		}
	}
	
	

	private int gridClampX(int t) {
		return MathHelpers.clamp(t,0,this.blockxcount-1);
	}
	
	private int gridClampY(int t) {
		return MathHelpers.clamp(t,0,this.blockycount-1);
	}
	
	private static ColorPoint getMinPointFromList(List<ColorPoint> pointList, double x, double y) {
		ColorPoint minPoint = null;
		double minDistSq = Double.MAX_VALUE;
		for(ColorPoint point:pointList) {
			double dx = point.x - x;
			double dy = point.y - y;
			
			double distSq = dx*dx + dy*dy;
			if(distSq<=minDistSq) {
				minPoint = point;
				minDistSq = distSq;
			}
		}
		
		return minPoint;
	}
	
	//TODO this could be made more efficient
	public ColorPoint getClosestPoint(double x, double y) {

		int startx = (int)(x/this.blockwidth);
		int starty = (int)(y/this.blockheight);
		
		
		for(int r=1;r<this.blockxcount||r<this.blockycount;r++) {
			int left = gridClampX(startx-r);
			int right = gridClampX(startx+r);
			
			int up = gridClampY(starty-r);
			int down = gridClampY(starty+r);
			
			
			List<ColorPoint> points = new LinkedList<ColorPoint>();
			for(int bx=left;bx<=right;bx++) {
				for(int by=up;by<=down;by++) {
					PointBlock point = blocks[bx][by];
					if(point!=null) {
						points.addAll(point.getPoints());
					}
				
				}
			}
			
			if(points.size()>0) {
				return getMinPointFromList(points, x, y);
			}

		}
		
		ColorPoint wrong = new ColorPoint();
		wrong.color = 0x10FF00;
		
		return wrong;
	}
	
	public PointBlock getPoints(double x, double y){
		return this.blocks[(int)(x/this.blockwidth)][(int)(y/this.blockheight)];
	}
}
