package voronoi;

import imagegenlib.annotate.CanonConstructor;
import imagegenlib.annotate.GenArgName;
import imagegenlib.annotate.GenClass;

@GenClass(
		name="Voronoi Noise Gaussian",
		desc="Creates a mosaic of random shaped and colored tiles, with more tiles occuring in the center"
)
public class VoronoiNoiseGaussian extends VoronoiNoise{

	@CanonConstructor
	public VoronoiNoiseGaussian(@GenArgName(name="Width") int width,@GenArgName(name="Height") int height,@GenArgName(name="Number of Tiles") int numpoints) {
		super(width,height,numpoints);
	}
	
	@Override
	protected double getPointPos(double max) {
		return max*0.5*(Math.random()+Math.random());
	}
}
