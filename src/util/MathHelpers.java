package util;

public final class MathHelpers {

	private MathHelpers() {}
	
	public static int clamp(int t, int min, int max) {
		return Math.max(min, Math.min(t, max));
	}
}
